# assign variables to the first fibonacci numbers
previous_0=0
previous_1=1
print("these are the first 18 fibonacci numbers")
print(previous_0)
print(previous_1)
# use a for loop that runs 18 times
for i in range(18):
    # new fibonacci number is the sum of the previous two
    new_fibonacci=previous_1+previous_0
    print(new_fibonacci)
    
    # update the previous fibonacci numbers
    previous_0=previous_1
    previous_1=new_fibonacci

